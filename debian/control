Source: doodle
Section: utils
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends: debhelper-compat (= 13),
               libextractor-dev,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://grothoff.org/christian/doodle/
Vcs-Browser: https://salsa.debian.org/debian/doodle
Vcs-Git: https://salsa.debian.org/debian/doodle.git

Package: doodle
Architecture: any
Depends: passwd,
         ${misc:Depends},
         ${shlibs:Depends},
Description: Desktop Search Engine (client)
 Doodle searches your hard drive for files using pattern matching on
 meta-data. It extracts file-format specific meta-data using
 libextractor and builds a suffix tree to index the files. The index
 can then be searched rapidly. It is similar to locate, but can take
 advantage of information such as ID3 tags. It is possible to do
 full-text indexing using the appropriate libextractor plugins.
 .
 This package contains the manual doodle client.

Package: libdoodle-dev
Section: libdevel
Architecture: any
Depends: libdoodle1 (= ${binary:Version}),
         ${misc:Depends},
Description: Desktop Search Engine (development)
 Doodle searches your hard drive for files using pattern matching on
 meta-data. It extracts file-format specific meta-data using
 libextractor and builds a suffix tree to index the files. The index
 can then be searched rapidly. It is similar to locate, but can take
 advantage of information such as ID3 tags. It is possible to do
 full-text indexing using the appropriate libextractor plugins.
 .
 This package contains the development files for the library to access
 the doodle database from other applications.

Package: libdoodle1
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
Pre-Depends: ${misc:Pre-Depends},
Description: Desktop Search Engine (library)
 Doodle searches your hard drive for files using pattern matching on
 meta-data. It extracts file-format specific meta-data using
 libextractor and builds a suffix tree to index the files. The index
 can then be searched rapidly. It is similar to locate, but can take
 advantage of information such as ID3 tags. It is possible to do
 full-text indexing using the appropriate libextractor plugins.
 .
 This package contains the shared library to access the doodle
 database from other applications.
